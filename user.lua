--│┌───────────────────────────────────────────────────────────────────────────────────────────┐
--││  █████╗  ██████╗  ███████╗ ███╗  ██╗  █████╗  ██████╗  ███╗   ███╗ ██╗ ███╗  ██╗  ██████╗││
--││ ██╔══██╗ ██╔══██╗ ██╔════╝ ████╗ ██║ ██╔══██╗ ██╔══██╗ ████╗ ████║ ██║ ████╗ ██║ ██╔════╝││
--││ ██║  ██║ ██████╔╝ █████╗   ██╔██╗██║ ███████║ ██║  ██║ ██╔████╔██║ ██║ ██╔██╗██║ ╚█████╗ ││
--││ ██║  ██║ ██╔═══╝  ██╔══╝   ██║╚████║ ██╔══██║ ██║  ██║ ██║╚██╔╝██║ ██║ ██║╚████║  ╚═══██╗││
--││ ╚█████╔╝ ██║      ███████╗ ██║ ╚███║ ██║  ██║ ██████╔╝ ██║ ╚═╝ ██║ ██║ ██║ ╚███║ ██████╔╝││
--││  ╚════╝  ╚═╝      ╚══════╝ ╚═╝  ╚══╝ ╚═╝  ╚═╝ ╚═════╝  ╚═╝     ╚═╝ ╚═╝ ╚═╝  ╚══╝ ╚═════╝ ││
--└───────────────────────────────────────────────────────────────────────────────────────────┘│

----------------------------------Либы

local com = require("component")
local computer=require("computer")
local event = require("event")
local term = require("term")
local shell = require("shell")
local fs = require("filesystem")
local unicode=require("unicode")
local serial = require("serialization")

----------------------------------Компоненты
 
local gpu = com.gpu
local color = gpu.setForeground
local rep = string.rep

----------------------------------Загрузка
 
color(0x00ff00)
term.clear()
gpu.setResolution(80,25)
print("\nНастройка библиотек...")
if not fs.exists("/lib/moskovLib.lua") then
    shell.execute("wget https://www.dropbox.com/s/hpsx9fa4jpbpxet/moskovLib.lua?dl=1 /lib/moskovLib.lua")
end

local mos = require("moskovLib")
print("\nИнициализация...")
print("Запуск программы...")



----------------------------------Конфиг

local maxAdmins = 14
local width, height = 95,28

local 		roles = {
					"&0[&4Гл. &1с&2у&3е&4т&5о&6л&aо&dг&0] - &0",				--{"&0[&9Гл.Модератор&0] - &3","moskovroma", "male"}, 
			  		"&0[&4Тех.Админ&0] - &0",		--[Админ] - moskovroma    [male]	[Online]	02:32:15
			  		"&0[&cАдминистратор&0] - &0",
			  		"&0[&9Гл.Модератор&0] - &3",
			  		"&0[&3Ст.Модератор&0] - &3",
			  		"&0[&5Строитель&0] - &d",
			  		"&0[&6Модератор&0] - &6",
			  		"&0[&2Помощник&0] - &2",
			  		"&0[&aСтажёр&0] - &2"}

local roles_table = {
					"&0[&4Гл. &1с&2у&3е&4т&5о&6л&aо&dг&0]",				
					"&0[&4Тех.Админ&0]",		
					"&0[&cАдминистратор&0]",
					"&0[&9Гл. Модератор&0]", --
					"&0[&3Ст. Модератор&0]",
					"&0[&5Строитель&0]",
					"&0[&6Модератор&0]",
					"&0[&2Помощник&0]",
					"&0[&aСтажёр&0]"}

local dataBD = shell.getWorkingDirectory() .. "/BD.txt" 

----------------------------------Глобальные переменные

local adminsData = {}
local logicSetting = false
local selAdmin = nil
local logicRole = false

----------------------------------Фронтэнд

function boxGlobalSmall()
    color(0x24b3a7)
    gpu.fill(1,1,1,height,'│')
    gpu.fill(2,1,1,height,'│')
    gpu.fill(width-1,1,1,height,'│')
    gpu.fill(width,1,1,height,'│')
    gpu.set(1,height,'└'..string.rep('─',width-3)..'┘'..'│')
    mos.centeringRight(width,height,"&3╢ &7Автор: moskovroma&3 ╟")    
    mos.centeringMid(width,1,"&3│┌────────────────────────────────────────────────────────────────────────────────────────────┐")
	mos.centeringMid(width,2,"&3││&c  █████╗  ██████╗  ███████╗ ███╗  ██╗  █████╗  ██████╗  ███╗   ███╗ ██╗ ███╗  ██╗  ██████╗ &3││")
	mos.centeringMid(width,3,"&3││&c ██╔══██╗ ██╔══██╗ ██╔════╝ ████╗ ██║ ██╔══██╗ ██╔══██╗ ████╗ ████║ ██║ ████╗ ██║ ██╔════╝ &3││")
	mos.centeringMid(width,4,"&3││&c ██║  ██║ ██████╔╝ █████╗   ██╔██╗██║ ███████║ ██║  ██║ ██╔████╔██║ ██║ ██╔██╗██║ ╚█████╗  &3││")
	mos.centeringMid(width,5,"&3││&c ██║  ██║ ██╔═══╝  ██╔══╝   ██║╚████║ ██╔══██║ ██║  ██║ ██║╚██╔╝██║ ██║ ██║╚████║  ╚═══██╗ &3││")
	mos.centeringMid(width,6,"&3││&c ╚█████╔╝ ██║      ███████╗ ██║ ╚███║ ██║  ██║ ██████╔╝ ██║ ╚═╝ ██║ ██║ ██║ ╚███║ ██████╔╝ &3││")
	mos.centeringMid(width,7,"&3││&c  ╚════╝  ╚═╝      ╚══════╝ ╚═╝  ╚══╝ ╚═╝  ╚═╝ ╚═════╝  ╚═╝     ╚═╝ ╚═╝ ╚═╝  ╚══╝ ╚═════╝  &3││")	
	gpu.set(3,8,'└'..string.rep('─',width-6)..'┐')	
end

function settingGUI( ... )
	if logicSetting then
		mos.printText(10,height,"&3╢ &8Настройки &3╟")
	else
		mos.printText(10,height,"&3╢ &4Настройки &3╟")
	end
	if #adminsData%2 ~= 0 then adminNum = #adminsData + 1 else adminNum = #adminsData end
	if logicSetting and selAdmin~=nil then
		mos.printText(10, height-4,"&3[ &5Сменить пол &3]")
		mos.printText(10,height-2,"&3[ &8Добавить &3]    &3[ &cУдалить &8]    &3[ &6Роль &3]    &3[ &4Выход &3]")
		mos.clearArea(7,9,3,17)
		mos.printText(7,(((height+6)/2)-(#adminsData/2)-1+selAdmin),"&8►►")
	elseif logicSetting and selAdmin==nil and #adminsData == maxAdmins then	
    	mos.clearArea(7,9,3,17)
    	mos.clearArea(10,height-3,22,1)
    	mos.printText(10,height-2,"&3[ &8Добавить &3]    &3[ &8Удалить &8]    &3[ &8Роль &3]    &3[ &4Выход &3]")
    elseif logicSetting and selAdmin==nil then
    	mos.clearArea(7,9,3,17)
    	mos.clearArea(10,height-3,22,1)
    	mos.printText(10,height-2,"&3[ &aДобавить &3]    &3[ &8Удалить &8]    &3[ &8Роль &3]    &3[ &4Выход &3]") --9   
    end
end

function printAdmins( ... )
	if #adminsData%2 ~= 0 then adminNum = #adminsData + 1 else adminNum = #adminsData end
	mos.clearArea(10,9,74,16)
	for i=1, #adminsData do
		mos.printText(10,(((height+6)/2)-(#adminsData/2)-1+i), adminsData[i][1]..adminsData[i][2])
		mos.printText(50,(((height+6)/2)-(#adminsData/2)-1+i), adminsData[i][3])
		mos.printText(70,(((height+6)/2)-(#adminsData/2)-1+i), isOnline(adminsData[i][2]))
	end
end

function printRoles( ... ) -- [ Гл.Модератор ]
	mos.clearArea(width/2 - 8,13,18,#roles_table)
	mos.drawBoxSmall(width/2 - 9,12,21,#roles_table+4,0x24b3a7)
	for i=1,#roles_table do
		mos.centeringMid(width,13+i, roles_table[i])
	end
end

----------------------------------Бекэнд

function loadData()
	if not (fs.exists(dataBD)) then
		shell.execute("wget https://pastebin.com/raw/rFCTwnnp /home/BD.txt")
	end
	file = io.open(dataBD, "r")
	local reads = file:read(9999999)
	if reads ~= nil then
	    adminsData = serial.unserialize("{" .. reads .. "}")
	else
	    adminsData = {}
	end
end

function saveData() -- Сохранение файла
    file = io.open(dataBD, "w")
    local text = ""
    for i = 1, #adminsData do
        text = text .. "{'"..adminsData[i][1].."','"..adminsData[i][2].."','"..adminsData[i][3].."'},\n"    
    end
    file:write(text)
    file:close()
    --io.open(path, 'w'):write(serialization.serialize(data)):close()
end

function isOnline(nick)
    result,reason=computer.addUser(nick)
    computer.removeUser(nick)
    if result==true then
        return "&8[&aОнлайн&8]"
    else
        return "&8[&4Оффлайн&8]"
    end
end

function adminAdd( ... )
	mos.printText(10,height-4,"&bВведите ник:&g")
	term.setCursor(24,height-4)
    local nick = io.read()
    nick, _ = nick:gsub("\n", "")
    if nick ~= "" then
    	for i = 1, #adminsData do
    		if adminsData[i][2] == nick then goto next end
    	end
    	table.insert(adminsData,{roles[#roles],nick,"&bMale"})
    end
    sort()
    saveData()
    ::next::
    mos.clearArea(10,height-4,string.len(nick)+14,1)
    boxGlobalSmall()
end

function adminGender( gender ) 
    if gender == "&bMale" then 
        return "&dFemale"
    elseif gender == "&dFemale" then
        return "&bMale"
    end
end

function delete( ... )
	table.remove(adminsData, selAdmin)
    selAdmin = nil
    sort()
    saveData()
end

function sort()
	local buffer = {}
	for i = 1, #roles do
		for j = 1, #adminsData do
			if roles[i] == adminsData[j][1] then
				table.insert(buffer, adminsData[j])
			end
		end
	end
	adminsData = buffer
end

function exit( ... )
	mos.clearArea(10,height-2,55,1)
	mos.clearArea(10,height-3,22,1)
	logicSetting = false
	selAdmin = nil
end

----------------------------------Нажатия

--[ Добавить ]    [ Удалить ]    [ Роль ]    [ Выход ]
function click(w,h)
	if #adminsData%2 ~= 0 then adminNum = #adminsData + 1 else adminNum = #adminsData end
	if pressButton(w,h,{10,height,13,1}) and not logicSetting then --Настройки
		logicSetting = true
	elseif pressButton(w,h,{13,(((height+6)/2)-(adminNum/2)),74,#adminsData}) and logicSetting then
		clickForAdmin(w,h)
	elseif pressButton(w,h,{10,height-4,15,1}) and selAdmin ~= nil then --[ Сменить пол ]
		adminsData[selAdmin][3] = adminGender(adminsData[selAdmin][3])
		selAdmin = nil
	elseif pressButton(w,h,{10,height-2,12,1}) and logicSetting and selAdmin==nil and (#adminsData < maxAdmins) then
		adminAdd()
	elseif pressButton(w,h,{27,height-2,11,1}) and selAdmin ~= nil and not logicRole then 
		delete()
	elseif pressButton(w,h,{54,height-2,9,1}) and logicSetting then
		exit()
	elseif pressButton(w,h,{42,height-2,8,1}) and selAdmin ~= nil and logicSetting and not logicRole then
		logicRole = true
		clickForRoles(w,h)	
	elseif not logicRole then
		selAdmin = nil
	end
end

function clickForAdmin(w,h)
	for i = 1, #adminsData do
		if pressButton(w,h,{13,(((height+6)/2)-(adminNum/2))-1+i,74,#adminsData}) then
			selAdmin = i
		end
	end
end

function clickForRoles()
	while logicRole do
		printRoles()
		local e,_,w,h,_,nick = event.pull(10, "touch")
		if e == "touch" then
			for i=1, #roles do
				if pressButton(w,h,{(width/2)-string.len(roles_table[i])/2,13+i,string.len(roles_table[i]),1}) then
					adminsData[selAdmin][1] = roles[i]
					selAdmin = nil
					logicRole = false
				elseif not pressButton(w,h,{width/2 - 9,12,21,#roles_table+4}) then
					selAdmin = nil
					logicRole = false
				end
			end
		end
	end
	sort()
	saveData()
end

function pressButton(Pw,Ph,mass)
    local x,y,w,h = mass[1], mass[2], mass[3], mass[4]
    if Pw>=x and Pw<=x+w-1 and Ph>=y and Ph<=y+h-1 then
        return true
    end
    return false
end

----------------------------------Запуск
term.clear()
gpu.setResolution(width, height)
loadData()
sort()
saveData()
boxGlobalSmall()
printAdmins()
while true do
	settingGUI()
	local e,_,w,h,_,nick = event.pull(10, "touch")
	if e == "touch" then
		click(w,h)
	end
	if sel == nil then
		printAdmins()
	end
	sort()
	saveData()
end

----------------------------------------------------
